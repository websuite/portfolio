import { TestBed, inject } from '@angular/core/testing';

import { PortfolioViewDataService } from './portfolio-view-data.service';

describe('PortfolioViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PortfolioViewDataService]
    });
  });

  it('should be created', inject([PortfolioViewDataService], (service: PortfolioViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
