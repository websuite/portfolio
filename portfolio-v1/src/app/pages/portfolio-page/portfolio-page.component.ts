import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-portfolio-page',
  templateUrl: './portfolio-page.component.html',
  styleUrls: ['./portfolio-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PortfolioPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
